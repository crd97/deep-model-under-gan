import os, time ,shutil
import matplotlib.pyplot as plt
import itertools
import pickle
import imageio
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch import autograd
from torchvision import datasets, transforms
from torch.autograd import Variable
import numpy as np
import random
from random import randrange as randr
import sys
import time

# training parameters
batch_size = 64
train_epoch = 200
use_cuda=True
attack_tag=[3]
fake_tag=[10]
attack=True
use_syft=0
device = torch.device("cuda" if use_cuda else "cpu")
use_defense=True
defense_r_list=[0.005,0.05,0.5,0.75,1,1.5,2]
use_dp=0#True
dp_clampn=0.001
use_dp_cost=True
dp_cost=0.01#10
use_dp_upload=True
dp_threshold=0.0001
r_upload=0.1
r_download=1

class Logger(object):
    def __init__(self, fileN="Default.log"):
        self.str=fileN
        self.terminal = sys.stdout
        self.log = open(fileN, "w")
 
    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)
        self.log.flush()
 
    def flush(self):
        self.log.flush()

    def __del__(self):
        print(self.str+"file closed")
        self.log.close()
        

# results save folder
print(time.strftime('%Y/%m/%d %H:%M:%S'))
timeStr=time.strftime('%y%m%d%H%M')
strFolder='MNIST_DCGAN_results'+timeStr
stdoutOri=sys.stdout
if not os.path.isdir(strFolder):
    os.mkdir(strFolder)
if not os.path.isdir(strFolder+'/Random_results'):
    os.mkdir(strFolder+'/Random_results')
if not os.path.isdir(strFolder+'/Fixed_results'):
    os.mkdir(strFolder+'/Fixed_results')
for filename in os.listdir(os.getcwd()):
    if filename.endswith('.py'):
        shutil.copy(filename,strFolder+'/'+filename+'_'+timeStr+'.py')
sys.stdout=Logger(strFolder+'/'+timeStr+'.txt')

if(use_dp):
    print("dp_clampn",dp_clampn)
    if use_dp_cost:
        print("dp_cost",dp_cost)
    if use_dp_upload:
        print("threshold",dp_threshold,"r_upload",r_upload,"r_download",r_download)
        
if use_syft:
    import syft as sy
    hook= sy.TorchHook(torch)
# G(z)
class generator(nn.Module):
    # initializers
    def __init__(self, d=128):
        super(generator, self).__init__()
        self.deconv1 = nn.ConvTranspose2d(100, 256, 4,1,0,bias=False)
        self.deconv1_bn = nn.BatchNorm2d(256)
        self.deconv2 = nn.ConvTranspose2d(256, 128, 4, 2, 1,bias=False)
        self.deconv2_bn = nn.BatchNorm2d(128)
        self.deconv3 = nn.ConvTranspose2d(128, 64, 4, 2, 1,bias=False)#pading might be 2
        self.deconv3_bn = nn.BatchNorm2d(64)
        self.deconv4 = nn.ConvTranspose2d(64, 1, 4, 2, 1,bias=False)

    # weight_init
    def weight_init(self, mean, std):
        for m in self._modules:
            normal_init(self._modules[m], mean, std)

    # forward method
    def forward(self, input):
        x = F.relu(self.deconv1_bn(self.deconv1(input)))
        x = F.relu(self.deconv2_bn(self.deconv2(x)))
        x = F.relu(self.deconv3_bn(self.deconv3(x)))
        x = torch.tanh(self.deconv4(x))
        return x

class discriminator(nn.Module):
    # initializers
    def __init__(self):
        super(discriminator, self).__init__()

        self.conv1=nn.Conv2d(1,32,kernel_size=5)
        self.conv2=nn.Conv2d(32,64,kernel_size=5)
        self.fc1=nn.Linear(256,200)
        self.fc2=nn.Linear(200,11)

    # weight_init
    def weight_init(self, mean, std):
        for m in self._modules:
            normal_init(self._modules[m], mean, std)

    # forward method
    def forward(self, x):
        x=F.max_pool2d(torch.tanh(self.conv1(x)),3)
        x=F.max_pool2d(torch.tanh(self.conv2(x)),2)
        x=x.view(-1,256)
        x=torch.tanh(self.fc1(x))
        x=self.fc2(x)
        return F.log_softmax(x,dim=1)

def normal_init(m, mean, std):
    if isinstance(m, nn.ConvTranspose2d) or isinstance(m, nn.Conv2d):
        m.weight.data.normal_(mean, std)
        #print(m.bias)
        if not m.bias is None:
            m.bias.data.zero_()

fixed_z_ = torch.randn_like(torch.ones(5 * 5, 100)).view(-1, 100, 1, 1).to(device)    # fixed noise

def show_result(num_epoch, show = False, save = False, path = 'result.png', isFix=False):
    torch.no_grad()
    z_ = torch.randn_like(torch.ones(5*5, 100)).view(-1, 100, 1, 1).to(device)

    attack_gen.eval()
    torch.no_grad()
    if isFix:
        test_images = attack_gen(fixed_z_)
    else:
        test_images = attack_gen(z_)
    attack_gen.train()
    torch.enable_grad()
    
    size_figure_grid = 5
    fig, ax = plt.subplots(size_figure_grid, size_figure_grid, figsize=(5, 5))
    for i, j in itertools.product(range(size_figure_grid), range(size_figure_grid)):
        ax[i, j].get_xaxis().set_visible(False)
        ax[i, j].get_yaxis().set_visible(False)

    for k in range(5*5):
        i = k // 5
        j = k % 5
        ax[i, j].cla()
        ax[i, j].imshow(test_images[k, 0].cpu().data.numpy(), cmap='gray')

    label = 'Epoch {0}'.format(num_epoch)
    fig.text(0.5, 0.04, label, ha='center')
    plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()

def show_train_hist(hist, tag_list,show = False, save = False, path = 'Train_hist.png',xlabel='Iter',ylabel='Loss',loc='upper right'):
    for tag in tag_list:
        x=range(len(hist[tag]))
        y = hist[tag]    
        plt.plot(x, y, label=tag)
    
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.legend(loc=loc)
    plt.grid(True)
    plt.tight_layout()

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()

#node define

if 0:#use_syft:
    # this is our hook
    me = hook.local_worker
    bob = sy.VirtualWorker(id='Bob',hook=hook,is_client_worker=False)
    alice = sy.VirtualWorker(id='Alice',hook=hook,is_client_worker=False)
else:
    alice="alice"
    bob="bob"
    me="me"
compute_nodes=[alice,bob]
#print("done")
#send data
node_tag={bob:[0,1,2,3,4],alice:[5,6,7,8,9]}
std_attack_data=None
attacker=alice
if attack_tag[0] in node_tag[alice]:
    attacker=bob
if attack:
    print("attacker:",attacker,'tag:',attack_tag)
# data_loader
img_size = 32
transform = transforms.Compose([
        transforms.Resize(img_size),
        transforms.ToTensor(),
        transforms.Lambda(lambda x:(x-torch.min(x))*2.0/(torch.max(x)-torch.min(x))-1) #[0,1]->[-1,1]
])
full_trainset=datasets.MNIST('data', train=True, download=True, transform=transform)
full_testset=datasets.MNIST('data', train=False, download=True, transform=transform)
node_train_index={bob:[],alice:[]}
process_print_interval=10000
size_lim=0
victim_index=[]
for batch_idx, (data, target) in enumerate(full_trainset):
    dl=data.tolist()
    if type(target)==int:
        tl=target
    else:
        tl=target.tolist()
    #if(batch_idx==0):
        #print(type(data))#<class 'torch.Tensor'>
        #print(type(target))#<class 'int'>
        #print(data.shape) #torch.Size([1, 32, 32])
    for node in compute_nodes:
        if tl in node_tag[node]:
            node_train_index[node].append(batch_idx)
    if tl in attack_tag:
        victim_index.append(batch_idx)
    if batch_idx%process_print_interval==0:
        print("extracting trainset:",batch_idx,"/",len(full_trainset))
        if size_lim>0 and batch_idx>=size_lim:
            break
bob_train_set=torch.utils.data.dataset.Subset(full_trainset,node_train_index[bob])
alice_train_set=torch.utils.data.dataset.Subset(full_trainset,node_train_index[alice])
victim_set=torch.utils.data.dataset.Subset(full_trainset,victim_index)
print('train set size： Bob:',len(bob_train_set),'Alice:',len(alice_train_set))
bob_train_loader = torch.utils.data.DataLoader(bob_train_set,batch_size=batch_size, shuffle=True)
alice_train_loader = torch.utils.data.DataLoader(alice_train_set,batch_size=batch_size, shuffle=True)
node_train_loader={bob:bob_train_loader,alice:alice_train_loader}
test_loader = torch.utils.data.DataLoader(datasets.MNIST('data', train=False, download=True, transform=transform),batch_size=batch_size, shuffle=True)#exit()

# network
model = discriminator().to(device)
bob_model=discriminator().to(device)
alice_model=discriminator().to(device)
attack_dis=discriminator().to(device)
attack_gen=generator().to(device)
#model.weight_init(mean=0.0, std=0.02)
#bob_model.weight_init(mean=0.0, std=0.02)
#alice_model.weight_init(mean=0.0, std=0.02)
attack_gen.weight_init(mean=0.0, std=0.02)

# Binary Cross Entropy loss
BCE_loss = nn.BCELoss()

arg_lr=1e-3
arg_mom=0
arg_decay=1e-7#/arg_lr
lr_lambda=lambda epoch: 1/(1+epoch*arg_decay)
bob_optim=optim.SGD(bob_model.parameters(), lr=arg_lr,momentum=arg_mom)#,weight_decay=arg_decay)
alice_optim=optim.SGD(alice_model.parameters(), lr=arg_lr,momentum=arg_mom)#,weight_decay=arg_decay)
attack_gen_optim=optim.SGD(attack_gen.parameters(), lr=0.02,momentum=0.5)#,weight_decay=arg_decay)
#attack_gen_optim= optim.Adam(attack_gen.parameters(), lr=0.0002, betas=(0.5, 0.999))
attack_dis_optim=optim.SGD(attack_dis.parameters(), lr=arg_lr,momentum=0)#,weight_decay=arg_decay)
bob_scheduler=optim.lr_scheduler.LambdaLR(bob_optim, lr_lambda)
alice_scheduler=optim.lr_scheduler.LambdaLR(alice_optim, lr_lambda)
#attack_gen_scheduler=optim.lr_scheduler.LambdaLR(attack_gen_optim, lr_lambda)
#attack_dis_scheduler=optim.lr_scheduler.LambdaLR(attack_dis_optim, lr_lambda)
node_model={bob:bob_model,alice:alice_model}
node_param={bob:list(bob_model.parameters()),alice:list(alice_model.parameters()),me:list(model.parameters()),"attack":list(attack_dis.parameters())}
node_optim={bob:bob_optim,alice:alice_optim}
node_scheduler={bob:bob_scheduler,alice:alice_scheduler}
node_dataset={bob:bob_train_set,alice:alice_train_set}

train_hist = {}
train_hist['D_real_losses'] = []
train_hist['D_fake_losses'] = []
train_hist['D_victim_losses'] = []
train_hist['gen_losses'] = []
train_hist['dis_losses'] = []
train_hist['test_set_accuracy'] = []
#train_hist['total_ptime'] = []
for f in defense_r_list:
    train_hist["defense_"+str(f)]=[]
num_iter = 0

print('training start!')
def train(epoch):
    #train
    torch.enable_grad()
    #global real_sample
    train_print_interval=80
    gen_train_print_interval=5
    global real_sample
    global fake_data_list
    for node_id,node in enumerate(compute_nodes):
        D_losses_real= []
        D_losses_fake=[0]
        gen_losses = [0]
        dis_losses=[0]
        D_real_batch=len(node_train_loader[node])
        G_batch=2#int(D_real_batch/10)#
        G_batch_size=batch_size
        D_fake_batch=4#10#int(D_real_batch/20)#100
        D_fake_batch_size=batch_size
        if epoch==0:
            print(node,"D_real_batch:",D_real_batch,"G_Batch:",G_batch,'*',G_batch_size,"D_fake_batch:",D_fake_batch,'*',D_fake_batch_size)
        if use_syft:
            node_model[node].send(node)
        if attack and attacker==node and epoch!=0:
            #print(node)
            D_losses_fake=[]
            gen_losses = []
            dis_losses=[]
            attack_gen.train()
            attack_dis.eval()
            if use_syft:
                attack_gen.send(node)
                attack_dis.send(node)
            for batch_id in range(G_batch):
                #------------attack-gen
                for f in range(1):
                    attack_gen.train()
                    attack_dis.eval()
                    attack_gen.zero_grad()
                    z_ = torch.randn((G_batch_size, 100)).view(-1, 100, 1, 1).to(device)                    
                    tar_G_=torch.LongTensor(attack_tag*G_batch_size).to(device)
                    if use_syft:
                        z_,tar_G_ = z_.send(node),tar_G_.send(node)
                    G_result = attack_gen(z_)
                    D_result = attack_dis(G_result)#.squeeze()
                    G_train_loss=F.nll_loss(D_result,tar_G_)
                    G_train_loss.backward()
                    attack_gen_optim.step()
                    gen_losses.append(G_train_loss.cpu().item())
                if batch_id%gen_train_print_interval==0 or batch_id==10:
                    print("epoch#{} node#{} G_train batch#{}/{} gen_loss={}".format(epoch+1,node_id,batch_id,G_batch,G_train_loss.cpu().item()))
                    #print("epoch#{} node#{} G_train batch#{}/{} gen_loss={} dis_loss={}".format(epoch+1,node_id,batch_id,G_batch,G_train_loss.cpu().item(),D_fake_loss.cpu().item()))
            print("epoch#{} node#{} gen_loss_ave={}".format(epoch+1,node_id,torch.mean(torch.FloatTensor(gen_losses))))
            #print("epoch#{} node#{} gen_loss_ave={} dis_loss_ave={}".format(epoch+1,node_id,torch.mean(torch.FloatTensor(gen_losses)),torch.mean(torch.FloatTensor(dis_losses))))
            attack_gen.eval()
            attack_dis.eval()     

        node_model[node].train()
        # train D with real
        real_sample_id=random.randrange(len(node_train_loader[node])-1)
        for batch_id,(x_, tar_) in enumerate(node_train_loader[node]):
            if batch_id>D_real_batch:
                break
            node_scheduler[node].step()
            #for p in node_model[node].parameters():
            #    p.data.clamp_(-CLAMPN, CLAMPN)
            #node_scheduler[node].step()
            node_model[node].zero_grad()
            mini_batch = x_.size()[0]
            x_=x_.to(device)
            y_real_ = tar_.to(device)#torch.ones((mini_batch,11))
            if(batch_id==real_sample_id)and attacker==node:
                real_sample=x_
            if use_syft:
                x_=x_.send(node)
            #print("x",x_)
            D_result = node_model[node](x_).squeeze()
            D_real_loss = F.nll_loss(D_result, y_real_)#was BCE_loss
            if 0:#batch_id==0:
                print("D_result:",D_result)
                print("y_real:",y_real_)
                print("D_real_loss",D_real_loss)
            D_losses_real.append(D_real_loss.cpu().item())
            D_real_loss.backward()
            node_optim[node].step()
            if batch_id%train_print_interval==0:
                print("epoch#{} node#{} D_train batch#{}/{} real_loss={}".format(epoch+1,node_id,batch_id,D_real_batch,D_real_loss.cpu().item()),end=' ')
                if 0:#attack and attacker==node and epoch!=0:
                    print('fake_loss={}'.format(D_fake_loss.cpu().item()),end=' ')
                print()
        print("epoch#{} node#{} D_real_loss_ave={}".format(epoch+1,node_id,torch.mean(torch.FloatTensor(D_losses_real))),end=' ')
        if 0:#attack and attacker==node and epoch!=0:
            print('fake_loss={}'.format(torch.mean(torch.FloatTensor(D_losses_fake))),end=' ')
        print()
        if use_syft:
            node_model[node].get()
           
        if attack and attacker==node and epoch!=0:
            fake_data_list=[] 
            node_model[node].train()
            attack_gen.eval()
            for batch_id in range(D_fake_batch):
                # #for p in node_model[node].parameters():
                # #    p.data.clamp_(-CLAMPN, CLAMPN)
                mini_batch=D_fake_batch_size
                z_ = torch.randn((mini_batch, 100)).view(-1, 100, 1, 1).to(device)
                G_result = attack_gen(z_).detach()
                # #print(G_result.size())
                fake_data_list.append(G_result)
            D_fake_batch=len(fake_data_list)
            for batch_id,G_result in enumerate(fake_data_list):
                if D_fake_batch_size==batch_size:
                    node_scheduler[node].step()
                mini_batch=G_result.size()[0]
                y_fake_ = torch.LongTensor([10]*mini_batch).to(device)#torch.zeros((mini_batch,11))
                if use_syft:
                    G_result,y_fake_=G_result.send(node),y_fake_.send(node)
                node_model[node].zero_grad()
                D_result = node_model[node](G_result)#.squeeze()
                D_fake_loss = F.nll_loss(D_result, y_fake_)#was BCE_loss
                D_fake_score = D_result.data.mean()
                D_losses_fake.append(D_fake_loss.cpu().item())
                D_fake_loss.backward()
                node_optim[node].step()
                if batch_id%gen_train_print_interval==0:
                    print("epoch#{} node#{} D_fake batch#{}/{} current_loss={}".format(epoch+1,node_id,batch_id,D_fake_batch,D_fake_loss.cpu().item()))
            print("epoch#{} node#{} D_fake_loss_ave={}".format(epoch+1,node_id,torch.mean(torch.FloatTensor(D_losses_fake))))
            if use_syft:
                attack_dis.get()
                attack_gen.get()
        #if attack and attacker==node and epoch!=0:
        #    print("epoch#{} node#{} D_fake_loss={}".format(epoch+1,node_id,torch.mean(torch.FloatTensor(D_losses_fake))))

        #prev_loss_G=torch.mean(torch.FloatTensor(gen_losses))
        #D_losses,G_losses=D_losses.cpu(),G_losses.cpu()
        if attacker==node:
            p = strFolder+'/Random_results/MNIST_DCGAN_' + str(epoch + 1) + '.png'
            fixed_p = strFolder+'/Fixed_results/MNIST_DCGAN_' + str(epoch + 1) + '.png'
            show_result((epoch+1), save=True, path=p, isFix=False)
            show_result((epoch+1), save=True, path=fixed_p, isFix=True)
            train_hist['D_real_losses'].append(torch.mean(torch.FloatTensor(D_losses_real)))
            train_hist['D_fake_losses'].append(torch.mean(torch.FloatTensor(D_losses_fake)))
            train_hist['gen_losses'].append(torch.mean(torch.FloatTensor(gen_losses)))
            train_hist['dis_losses'].append(torch.mean(torch.FloatTensor(dis_losses)))
            #train_hist['per_epoch_ptimes'].append(per_epoch_ptime)
        else:
            train_hist['D_victim_losses'].append(torch.mean(torch.FloatTensor(D_losses_real)))            
            
    #--upload
    #print(len(list(model.parameters())),len(list(bob_model.parameters())))
    new_param_list=list()
    for param in model.parameters():
        new_param_list.append(param.data)
    for node in compute_nodes:
        if use_dp_upload:
            mask=[]
            candidate=[]#parameters above threshold
            total=0
            lap_1=2*(2*dp_clampn)/(dp_cost*8/9.0)
            #if epoch==0:
            #    print("lap_1",lap_1)
            for param_i,param in enumerate(node_param[node]):
                delta=node_param[node][param_i].data-node_param[me][param_i].data
                numel=param.numel()
                #if(epoch==0):
                #    print("numel",numel,"total",total,time.time()) 
                total=total+numel
                mask.append(torch.zeros(numel).cpu())#gpu would be too slow
                absv=delta.view(-1).clamp(-dp_clampn,dp_clampn)+torch.Tensor(np.random.laplace(scale=(2*lap_1),size=numel)).to(device)#gpu is slower
                absv=torch.abs(absv)
                lim=torch.Tensor(np.random.laplace(loc=dp_threshold,scale=lap_1,size=numel)).to(device)
                cmpa=torch.gt(absv,lim).cpu()
                for i in range(numel):                    
                    if(cmpa[i].item()>0):#(absv[i].item()>lim[i].item()):
                        candidate.append((param_i,i))
                #if epoch==0:
                #    print(node,param_i,'#',"lap",lap_1)
                #    print(node,param_i,'#','param',param.data)
                #    print(node,param_i,'#','numel',numel,total)
                #    print(node,param_i,'#','absv',absv)
                #    print(node,param_i,'#','lim',lim)
                #    print(node,param_i,'#','candidate_count',len(candidate))
            selected=random.sample(candidate,min(int(total*r_upload),len(candidate)))
            #if epoch==0:
            #    print(node,'#','len_slected',len(selected))
                #print(node,'#','slected',selected)
            for (param_i,i) in selected:
                mask[param_i][i]=1
        for param_i,param in enumerate(node_param[node]):
            delta=node_param[node][param_i].data-node_param[me][param_i].data
            #if epoch==0:
            #    print(node,param_i,'#','delta_raw',delta)
            if use_dp:
                if use_dp_cost:
                    lap_2=2*(2*dp_clampn)/(dp_cost/9.0)
                    #if epoch==0:
                    #    print("lap_2",lap_2)
                    delta=delta+torch.Tensor(np.random.laplace(scale=lap_2,size=delta.size())).to(device)
                if use_dp_upload:
                    delta=delta*(mask[param_i].view_as(delta).to(device))
                delta.clamp_(-dp_clampn,dp_clampn)
            new_param_list[param_i]=new_param_list[param_i]+delta
            #if epoch==0:
            #    print(node,param_i,'#',"lap",lap_2)
            #    print(node,param_i,'#','delta',delta)
            #    print(node,param_i,'#','new_param',new_param_list[param_i])
           
    for node in compute_nodes+[me]:
        for param in node_param[node]:
            param.data*=0

    #--download
    for node in compute_nodes+[me,"attack"]:
        for param_index in range(len(node_param[node])):
            node_param[node][param_index].data.set_(new_param_list[param_index].clone())

def test(epoch):
    model.eval()
    test_loss = 0
    correct = 0
    cnt=0
    test_distribution={}
    torch.no_grad()
    for batch_id, (data, target) in enumerate(test_loader):
        if use_cuda:
            data, target = data.cuda(), target.cuda()
        #print(pred.tolist(),target.tolist(),correct,cnt)
        tar_list=target.tolist()
        #data, target = Variable(data, volatile=True), Variable(target)
        #output = node_model[bob](data)
        output=model(data)
        test_loss += F.nll_loss(output, target, reduction="sum").item() # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        pred_list=pred.tolist()
        if(type(tar_list)!=list):
            tar_list,pred_list=[tar_list],[pred_list]
        #if batch_id==1:
        #    print(tar_list)
        #    print(pred_list)
        correct += pred.eq(target.data.view_as(pred)).long().cpu().sum()
        for i in range(len(tar_list)):
            tar_num,pred_num=tar_list[i],pred_list[i][0]
            if not(tar_num,pred_num) in test_distribution.keys():
                test_distribution[(tar_num,pred_num)]=1
            else:
                test_distribution[(tar_num,pred_num)]=test_distribution[(tar_num,pred_num)]+1
            cnt=cnt+1
    #cnt=10000#len(test_set)
    test_loss /= cnt
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)'.format(
        test_loss, correct, cnt,100. * correct / cnt))
    print("Distribution",test_distribution ,'\n')
    train_hist['test_set_accuracy'].append(1.0*float(correct)/cnt)
    return 1. * float(correct) / cnt

class DefenseDataset(torch.utils.data.Dataset):
    def __init__(self,picList,tar):
        self.picList=picList
        self.tar=tar
    def __getitem__(self, index):
        return self.picList[index],self.tar
    def __len__(self):
        return len(self.picList)
    
def defense(tar,victim_set,noise_ratio=2,defense_batch=2000):
    #print(tar)
    #print(std_data)
    model.eval()
    defense_loss = 0
    correct = 0
    defense_error_distribution={}
    torch.no_grad()
    defense_list=[]
    for batch_id in range(defense_batch):
        noise = torch.randn(1,img_size,img_size)#was 1*img_size*img_size
        if 0:#batch_id==1:
            print("noise:",noise)
            print("std:",std_data)
        std_data=victim_set[randr(len(victim_set))][0].view(1,img_size,img_size)
        data=std_data+noise*noise_ratio
        #func=nn.BatchNorm2d(1)#,affine=False,track_running_stats=False)
        #data=data_raw
        #data=transforms.Normalize((0.1307,), (0.3081,))(data)
        defense_list.append(data)
    defense_set=DefenseDataset(defense_list,tar)
    defense_loader= torch.utils.data.DataLoader(defense_set,batch_size=batch_size, shuffle=True)    
    for batch_id,(data,target) in enumerate(defense_loader):
        if use_cuda:
            data, target = data.cuda(), target.cuda()
        #print(pred.tolist(),target.tolist(),correct,cnt)
        tar_num=target.tolist()[0]
        #data, target = Variable(data, volatile=True), Variable(target)
        #output = node_model[bob](data)
        output=model(data)
        defense_loss += F.nll_loss(output, target, reduction="sum").item() # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        pred_list,tar_list=pred.tolist(),target.tolist()
        if(type(tar_list)!=list):
            tar_list,pred_list=[tar_list],[pred_list]
        correct += pred.eq(target.data.view_as(pred)).long().cpu().sum()
        for i in range(len(tar_list)):
            tar_num,pred_num=tar_list[i],pred_list[i][0]
            if(tar_num==pred_num):
                continue
            if not(tar_num,pred_num) in defense_error_distribution.keys():
                defense_error_distribution[(tar_num,pred_num)]=1
            else:
                defense_error_distribution[(tar_num,pred_num)]=defense_error_distribution[(tar_num,pred_num)]+1

    print('Defensing: noise_ratio=',noise_ratio,' Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)'.format(
        defense_loss/defense_batch, correct, defense_batch,100. * correct / defense_batch))
    print("Error Distribution",defense_error_distribution)
    return 1.0*float(correct)/defense_batch

start_time = time.time()
prev_loss_G=0
fake_data_list=[]
for epoch in range(train_epoch):
    epoch_start_time = time.time()
    print("----- epoch #{} -----".format(epoch+1))
    train(epoch)
    accuracy=test(epoch)
    if 1:#epoch>150:
        for r in defense_r_list:
            train_hist['defense_'+str(r)].append(defense(attack_tag[0],victim_set,noise_ratio=r,defense_batch=200))
    epoch_end_time = time.time()
    per_epoch_ptime = epoch_end_time - epoch_start_time
    print('epoch #%d/%d - ptime: %.2f' % ((epoch + 1), train_epoch, per_epoch_ptime))
    if epoch%20==0:
        show_train_hist(train_hist,['D_real_losses','D_fake_losses','gen_losses','test_set_accuracy'], save=True, path=strFolder+'/MNIST_DCGAN_train_hist.png')
        show_train_hist(train_hist, ['test_set_accuracy'],
            save=True, path=strFolder+'/accuracy_'+timeStr+'.png',ylabel='accuracy')
        for r in defense_r_list:
            show_train_hist(train_hist, ['defense_'+str(r)],save=True, path=strFolder+'/defense'+str(r)+'_'+timeStr+'.png',ylabel='accuracy',loc="lower right")
    #if accuracy>0.97:
    #    break
end_time = time.time()
total_ptime = end_time - start_time
#train_hist['total_ptime'].append(total_ptime)

#print("Avg per epoch ptime: %.2f, total %d epochs ptime: %.2f" % (torch.mean(torch.FloatTensor(train_hist['per_epoch_ptimes'])), train_epoch, total_ptime))
print("Training finish!... save training results")
#torch.save(G.state_dict(), "MNIST_DCGAN_results/generator_param.pkl")
#torch.save(D.state_dict(), "MNIST_DCGAN_results/discriminator_param.pkl")
#with open(strFolder+'/train_hist.pkl', 'wb') as f:
#    pickle.dump(train_hist, f)

show_train_hist(train_hist,['D_real_losses','D_fake_losses','D_victim_losses','gen_losses','test_set_accuracy'], save=True, path=strFolder+'/MNIST_DCGAN_train_hist.png')
show_train_hist(train_hist, ['test_set_accuracy'],
    save=True, path=strFolder+'/accuracy_'+timeStr+'.png',ylabel='accuracy')
for r in defense_r_list:
    show_train_hist(train_hist, ['defense_'+str(r)],save=True, path=strFolder+'/defense'+str(r)+'_'+timeStr+'.png',ylabel='accuracy',loc="lower right")
#images = []
#for e in range(train_epoch):
#    img_name = strFolder+'/Fixed_results/MNIST_DCGAN_' + str(e + 1) + '.png'
#    images.append(imageio.imread(img_name))
#imageio.mimsave(strFolder+'/generation_animation.gif', images, fps=5)
sys.stdout=stdoutOri


